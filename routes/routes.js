
const express = require('express');
const app = express();
const port = 3000;
const userModel = require("./models");

// /// CMD:$ CHCP 1258 để CMD đọc được tiếng việt
// //mở port
// // app.listen(3000, () => console.log(`đã mở port 3000`))
// app.listen(port, () => {
//   console.log(`Đã mở port ${port} hãy truy cập trên trình duyệt`);
// });

// respond with "hello world" when a GET request is made to the homepage
app.get('/', (req, res) => {
  res.send('welcome to home page')
})

// ...
app.post("/add_user", async (request, response) => {
  const user = new userModel(request.body);
  console.log(request.body);
  try {
    await user.save();
    response.send(user);
  } catch (error) {
    response.status(500).send(error);
  } 
});

// ...
app.get("/users", async (request, response) => {
  const users = await userModel.find({});

  try {
    response.send(users);
  } catch (error) {
    response.status(500).send(error);
  }
});


// var test = require('../views/user');
// app.get('/users', test.list);
// app.get('/user/:id', test.view);

// var inpost = require('../views/create')
// app.post('/views/create',  inpost.post);
// ...
module.exports = app;