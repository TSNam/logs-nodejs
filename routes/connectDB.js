const express = require("express");
const mongoose = require("mongoose");
const MongoClient = require('mongodb').MongoClient;
const Router = require("./routes");
const app = express();

app.use(express.json());

mongoose.connect("mongodb://localhost:27017/logs",
  {
    useNewUrlParser: true,
    // useFindAndModify: false,
    useUnifiedTopology: true
  }
);

// ...
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

// ...
app.use(Router);

app.listen(3000, () => {
  console.log("Server is running at port 3000");
});
// --------------------------------------------------------
// const { MongoClient } = require('mongodb');
// // or as an es module:
// // import { MongoClient } from 'mongodb'

// // Connection URL
// const url = 'mongodb://localhost:27017';
// const client = new MongoClient(url);

// // Database Name
// const dbName = 'logs';
// let collection = null;

// async function main() {
//   // Use connect method to connect to the server
//   await client.connect();
//   console.log('Connected successfully to server');
//   const db = client.db(dbName);
//   collection = db.collection('documents');

//   // the following code examples can be pasted here...

//   return 'done.';
// }

// async function test_insert() {
//   const insertResult = await collection.insertMany([{ a: 1 }, { a: 2 }, { a: 3 }]);
//   console.log('Inserted documents =>', insertResult);
// }

// main()
//   .then(function() {
//     test_insert();
//   })
//   .catch(console.error)
//   .finally(() => client.close());
