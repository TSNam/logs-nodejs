const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  action: {
    type: String,
    required: true,
  },
  user_id: {
    type: Number,
    default: 0,
  },
  metadata: {
    type: String,
  },
  content: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
});
const User = mongoose.model("User", UserSchema);

module.exports = User;